source ./tests/unittests/base.sh


# Tests
testExecuteHook() {
    test() {
        start_subtest "Check that hooks can execute multiple commands"
        logs=`execute_hooks "My hook test" "touch /tmp/file1\ntouch /tmp/file2\n"`
        assertEquals 0 $?
        assertTrue 'The first file was not created' "[ -f /tmp/file1 ]"
        assertTrue 'The second file was not created' "[ -f /tmp/file2 ]"
        assertContains "$logs" "section_start"
        assertContains "$logs" "section_end"

        start_subtest "Check what happens upon failure"
        logs=`execute_hooks "My hook test" "touch /missing/folder"`
        assertEquals 1 $?
        assertContains "$logs" "section_start"
        assertContains "$logs" "section_end"
    }

    run_unit_test test
}
suite_addTest testExecuteHook
